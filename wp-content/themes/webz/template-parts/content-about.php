<?php 
	/*
		Template Name: About
	*/
?>
	<?php get_header(); ?>
	<div class="body_bg background_white">
        <div class="page_img margin_top_page_mb">
            <div class="wallpaper_img"></div>
            <img src="<?php echo IMAGES; ?>/About_02.jpg" title="About us">
        </div>

        <div class="container page_bg padding_bottom">     
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 project_menu">
                <div class="fixed_menu_bg" style="position: static;">                                       
                    <div class="split_bar_news_top"><div class="page_title_menu_left hidden-xs hidden-sm">GIỚI THIỆU</div></div>
                    <div class="left_menu_bg">
                        
                        <a class="left_menu_item selected" data-id="0">GIỚI THIỆU CHUNG<span></span></a>
                        
                        <a class="left_menu_item" data-id="1">TẦM NHÌN &amp; SỨ MỆNH<span></span></a>
                        
                        <a class="left_menu_item" data-id="2">SƠ ĐỒ TỔ CHỨC<span></span></a>
                        
                        <a class="left_menu_item" data-id="3">HỢP TÁC ĐẦU TƯ<span></span></a>
                        
                    </div>
                </div>
            </div>
            <!-- end col-lg 3 -->
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">   
                <div class="tab_content_item_bg" style="display: block;">
                    <div class="page_title">GIỚI THIỆU CHUNG</div>
                    <div class="page_content">
<!--                        <div class="about_title"><b>CÔNG TY CỔ PHẦN DỊCH VỤ ĐỊA ỐC GBL</b></div>-->
                        <div>
                          <?php the_field('gioi_thieu_chung'); ?>
                        </div>

                    </div>
                </div>           
                <div class="tab_content_item_bg" style="display: none;">
                    <div class="page_title">TẦM NHÌN &amp; SỨ MỆNH</div>
                    <div class="page_content">
<!--                    <div class="about_title">Sứ mệnh CỔ PHẦN DỊCH VỤ ĐỊA ỐC GBL đề ra là:</div>-->

                            <?php the_field('tam_nhin_va_su_menh'); ?>

                    </div>
                </div>           
                <div class="tab_content_item_bg" style="display: none;">
                    <div class="page_title">SƠ ĐỒ TỔ CHỨC</div>
                        <div class="page_content">
                            <?php the_field('so_do_to_chuc'); ?>
                            <div>
                    </div>
                    <div style="text-align: center; ">&nbsp;</div>
                    <div>
                       
                    </div>
                </div> 
                </div>	
                <div class="tab_content_item_bg" style="display: none;">
                    <div class="page_title">HỢP TÁC ĐẦU TƯ</div>
                        <div class="page_content">
                            <?php the_field('hop_tac_dau_tu'); ?>

                    	</div>
                	</div>         
            	</div>
        	</div> 
            <script>
                $(document).ready(function(){
                    $('.left_menu_item').click(function() {
                        $('.left_menu_item').removeClass('selected');
                        $(this).toggleClass('selected');
                        var x = $(this).attr('data-id');
                        $('.tab_content_item_bg').css('display','none');
                        $(".tab_content_item_bg:eq("+x+")").css('display','block');
                    });
                })
            </script>   
    	</div>
    <?php get_footer(); ?>