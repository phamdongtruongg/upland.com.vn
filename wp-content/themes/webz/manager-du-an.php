<?php 
	add_action('init','create_custom_post_type_du_an');
	function create_custom_post_type_du_an(){
		$args = array(
			'labels' => array(
	           	'name' => __( 'Quản lý dự án' ),
	            'singular_name' => __( 'dự án' )
	        ),
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt'
			),
			'rewrite' => array('slug' => 'du-an', 'with_front' => true)
		);
	   register_post_type('du-an', $args);

	   $args_taxonomy = array(
			'hierarchical' => true,
			'label' => 'Danh mục dự án',
			'rewrite' => true,
			'slug' => 'danh-muc'
		);
		register_taxonomy('danh-muc',array('du-an'),$args_taxonomy);
	}
function wpse_post_types_admin_duan($wp_query){
	if (is_admin()) {
		$post_type = $wp_query->query['post_type'];
		if ($post_type == 'du-an' || $post_type == 'tin-tuc') {
			$wp_query->set('orderby', 'date');
			$wp_query->set('order', 'DESC');
		}
	}
}
add_filter('pre_get_posts','wpse_post_types_admin_duan');