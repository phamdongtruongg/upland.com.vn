 <?php get_header(); ?>
<div class="body_bg background_white">

    <div class="page_img margin_top_page_mb">
        <div class="wallpaper_img"></div>
        <img src="<?php echo IMAGES; ?>/HR_01.jpg">
    </div>
    
    <div class="container page_bg padding_bottom">
        
        <div class="project_menu col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <div class="fixed_menu_bg" style="position: static; width : calc(100% - 30px)">
                <div class="row">

                    <div class="hotline_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                        <div class="info_content info_hotline">
                            <b>CHUYÊN GIA TƯ VẤN BẤT ĐỘNG SẢN ĐỖ DUY TUẤN</b><br>
                            Địa chỉ: 915 Ngô Quyền, Phường An Hải Đông
                            Quận Sơn Trà, Thành phố Đà Nẵng
                        </div>
                        <div class="img_hotline"></div>
                        <div class="info_hotline">
                            Hotline: <a href="tel:0901627292"><span>0901.62.72.92</span></a><br>
                            <a href="mailto:Datvangmientrung.bds@gmail.com">Datvangmientrung.bds@gmail.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <?php while (have_posts()) : the_post(); $post_id = get_the_ID(); ?>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            
            <div class="tab_content_item_bg" style="display: block;"> 
                <div class="page_title"><a href="<?php echo URL; ?>/du-an">Tuyển dụng</a> <span></span><?php the_title(); ?></div>
                <div class="page_content">
                    <div class="page_break"></div>
                    <div class="slider-wrapper theme-default project_slide_bg"></div>
                </div>
            </div>
            
            <div class="page_content_detail project">                      
                <?php the_content(); ?> 
            </div>

        <?php 
            $danh_muc = get_the_terms( $post_id, 'danh-muc');
            $args = array(
                'post_type' => 'du-an',
                'posts_per_page' => 10,
                'post__not_in' => array($post_id),
                'orderby'          => 'date',
                'order'            => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'danh-muc',
                        'field' => 'term_id',
                        'terms' => $danh_muc[0]->term_id
                    )
                )
            );
            $projectlist = get_posts( $args );
        ?>
        <?php if (!empty($projectlist)) : ?>
            <div class="news_other_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding_right">
                <div class="split_bar_news_top"><div class="news_other_title">Các dự án khác</div></div>
                <div class="news_other_item_bg">

                    <?php foreach ($projectlist as $project) : ?>
                        <a href="<?php echo get_the_permalink($project->ID); ?>" class="bubble-float-right "><?php echo $project->post_title; ?><span> ( <?php echo get_the_time('d/m/Y', $project->post_ID ); ?> )</span></a><br class="">
                    <?php endforeach; ?>
                </div>
                <a href="<?php echo URL; ?>/du-an" class="other_new_readmore_btn">Xem thêm</a>  

            </div>
        <?php endif; ?>
        </div>
        <?php endwhile; ?>
        <div class="request text-right">
            <a href="/lien-he" target="_blank">Gởi yêu cầu</a>
        </div>
    </div>    
</div>

<?php get_footer(); ?>