<?php get_header(); ?>
	<div class="body_bg background_white">
        <a href="" target="_blank" title="Google Maps" class="page_img contact_map">
            <img src="<?php echo IMAGES; ?>/Contact_01.jpg" class="margin_top_page_mb" title="Contact us">
        </a>

        <div class="container page_bg padding_bottom">     
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="page_title contact">Liên hệ</div>
            </div>
            <div class="contact_tab_btn_bg">
                <a class="contact_tab_btn_item office selected" data-id="0">Văn phòng</a>
                <a class="contact_tab_btn_item message" data-id="1">Gởi mail</a>
            </div>            
            <div class="contact_tab_bg contact_info_bg col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: block;">
                <div class="page_content_detail">
                    <span>CÔNG TY CỔ PHẦN ĐẤT XANH MIỀN TRUNG</span><br>
                    <div class="detail-info" style="text-align: left;">
                        <strong>Trụ sở</strong>: 438 đường 2-9, Phường Hòa Cường Bắc, Quận Hải Châu, TP. Đà Nẵng<br>
                        <strong>Điện thoại</strong>: (84) 02366 266 266<br>
                        <strong>Fax</strong>: (84) 02366 260 260<br>
                        <strong>Email</strong>:&nbsp;<a href="mailto:doduytuan2012@gmail.com.vn">doduytuan2012@gmail.com.vn</a><br>
                        <strong>Chi nhánh</strong>: 386 Điện Biên Phủ, Phường Hòa Khê, Quận Thanh Khê, TP. Đà Nẵng<br>
                        <strong>Điện thoại</strong>: (84) 02363 522 055<br>
                        <strong>Fax</strong>: (84) 02363 522055  
                    </div>
                </div>                        
            </div>        
            <div class="contact_tab_bg contact_form_bg col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: none;">                    
                <div class="contact_form_content_bg">                                      
                    <div class="contact_txt_bg">
                        <input type="text" id="txt_contact_name" class="contact_txt" placeholder="Họ tên"><label id="name_error_mes"></label>
                    </div>
                    <div class="contact_txt_bg">
                        <input type="text" id="txt_contact_email" class="contact_txt" placeholder="Email"><label id="email_error_mes"></label>
                    </div>
                    <div class="contact_txt_bg">
                        <input type="text" id="txt_contact_phone" class="contact_txt" placeholder="Điện thoại"><label id="phone_error_mes"></label>
                    </div>
                    <div class="contact_txt_bg">
                        <input type="text" id="txt_contact_address" class="contact_txt" placeholder="Địa chỉ"><label id="address_error_mes"></label>
                    </div>            
                    <div class="contact_txta_bg">
                        <textarea id="txt_contact_content" class="contact_txta" placeholder="Nội dung"></textarea>            
                        <label id="content_error_mes"></label>
                    </div>            
                </div>               
                <div class="contact_button">
                    <a class="contact_button_send"><span>Gởi</span></a> 
                </div>
            </div>
            <script>
                $(document).ready(function(){
                    $('.contact_tab_btn_item').click(function() {
                        $('.contact_tab_btn_item').removeClass('selected');
                        $(this).toggleClass('selected');
                        var x = $(this).attr('data-id');
                        $('.contact_tab_bg').css('display','none');
                        $(".contact_tab_bg:eq("+x+")").css('display','block');
                    });
                })
            </script>         
        </div>    
    </div>
<?php get_footer(); ?>