<?php get_header(); ?>
	<div class="body_bg background_white">
        <a href="" target="_blank" title="Google Maps" class="page_img contact_map">
            <img src="<?php echo IMAGES; ?>/Contact_01.jpg" class="margin_top_page_mb" title="Contact us">
        </a>

        <div class="container page_bg padding_bottom">     
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="page_title contact">Liên hệ</div>
            </div>
            <div class="contact_tab_btn_bg">
                <a class="contact_tab_btn_item office selected" data-id="0">Trực tiếp</a>
                <a class="contact_tab_btn_item message" data-id="1">Gởi mail</a>
            </div>    
                  
            <div class="contact_tab_bg contact_info_bg col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: block;">
                <div class="row page_content_detail">
                    
                    <div class="col-lg-6 col-md-4 imgContact">
                        <img src="<?php echo IMAGES; ?>/ddtuan.jpg" alt="">
                    </div> 
                    <div class="col-lg-6 col-md-8">
                        <span>CHUYÊN GIA TƯ VẤN BẤT ĐỘNG SẢN ĐỖ DUY TUẤN</span><br>
                        <div class="detail-info" style="text-align: left;">
                            <strong>Địa chỉ</strong>: 438 đường 2-9, Phường Hòa Cường Bắc, Quận Hải Châu, TP. Đà Nẵng<br>
                            <strong>Điện thoại</strong>: 0905 813 582 - 0901 997 046<br>
                            <strong>Email</strong>:&nbsp;<a href="mailto:doduytuan2012@gmail.com.vn">doduytuan2012@gmail.com.vn</a><br>
                            <strong>Website</strong>: doduytuanbds.com<br>
                        </div>
                    </div>
                </div>                        
            </div>        
            <div class="contact_tab_bg contact_form_bg col-lg-4 col-lg-offset-4 col-md-8 col-md-offset-2 col-sm-12 col-xs-12" style="display: none;">                    
            <?php the_content(); ?> 
            </div>
            
            <script>
                $(document).ready(function(){
                    $('.contact_tab_btn_item').click(function() {
                        $('.contact_tab_btn_item').removeClass('selected');
                        $(this).toggleClass('selected');
                        var x = $(this).attr('data-id');
                        $('.contact_tab_bg').css('display','none');
                        $(".contact_tab_bg:eq("+x+")").css('display','block');
                    });
                })
            </script>         
        </div>    
    </div>
   
<?php get_footer(); ?>