<?php get_header(); ?>
<div class="body_bg background_white">
    <div class="page_img margin_top_page_mb">
        <div class="wallpaper_img"></div>
        <img src="<?php echo IMAGES; ?>/HR_01.jpg">
    </div>
    <div class="container page_bg padding_bottom">     
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 project_menu">
            <div class="fixed_menu_bg">                                     
                <div class="split_bar_news_top">
                    <div class="page_title_menu_left hidden-xs hidden-sm">TUYỂN DỤNG</div>
                </div>
                <a href="" class="left_menu_rec_item selected">Tin tuyển dụng<span></span></a>
                <div class="left_menu_bg">
                    
                    <a href="" class="left_menu_rec_item">Chính sách nhân sự<span></span></a>
                    
                    <a href="" class="left_menu_rec_item">Định hướng nghề nghiệp<span></span></a>
                    
                    <a href="" class="left_menu_rec_item">Quy trình tuyển dụng<span></span></a>
                                        
                    <a href="<?php echo URL; ?>/lien-he" class="left_menu_rec_item">Thông tin liên hệ<span></span></a>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">           
            <div class="news_item">
                <a href="" class="news_img col-lg-4 col-md-4 col-sm-12 col-xs-12 no_padding_right">
                    <img src="<?php echo IMAGES; ?>/hr/TD-02.jpg">
                </a>
                <div class="news_info col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <a href="" class="news_title">CHUYÊN GIA TƯ VẤN BẤT ĐỘNG SẢN ĐỖ DUY TUẤN</a>
                    <div class="split_bar_news"></div>
                    <div class="news_content des_string">
                        Bạn đam mê công việc kinh doanh, bạn đam mê kiếm tiền? <br>Bạn hào hứng chinh phục những thách thức về doanh số cá nhân? <br>Bạn muốn nắm bắt cơ hội làm giàu và phát triển bản thân?<br>Bạn có niềm đam mê Bất động sản?
                    </div>                    
                </div>                   
            </div>
        </div>
    </div>    
</div>
<?php get_footer(); ?>