<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
    	<meta charset="<?php bloginfo( 'charset' ); ?>">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="keywords" content="Bất động sản Đỗ Duy Tuấn" />
        <meta name="Description" content="Website bất động sản Đỗ Duy Tuấn" />
        <link rel="shortcut icon" href="<?php echo IMAGES; ?>/logo-new.png" type="image/x-icon" />
        <!-- STYLE FILE -->
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/template.css"/>
        	<link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/gray.min.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/pages.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/menu.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/carousel.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/tablet.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/mobile.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/jquery-ui-1.10.3.custom.min.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/bootstrap.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/nivoslider.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/default.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/jquery-ui.min.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/jquery.fancybox.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/nprogress.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/selectbox.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/style.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo PATH; ?>/css/contact.css"/>
        <!-- END STYLE FILE -->

        <!-- SCRIPT FILE -->
            
        	<script type="text/javascript" src="<?php echo PATH; ?>/js/jquery-1.11.1.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery-ui.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery-migrate-1.2.1.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery-ui-1.10.3.custom.min.js" ></script>        
            <script type="text/javascript" src="<?php echo PATH; ?>/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/bootstrap-debugger.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.touchSwipe.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.cookie.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.easing.1.3.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.mousewheel.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.gray.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/modernizr.custom.js"></script>        
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.fancybox.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jcarousellite.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.autoNumeric.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.ba-hashchange.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/nprogress.js"></script>       
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.nivo.slider.pack.js"></script>         
            <script type="text/javascript" src="<?php echo PATH; ?>/js/imagesloaded.pkgd.min.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.transit.min.js"></script> 
            <script type="text/javascript" src="<?php echo PATH; ?>/js/jquery.selectbox.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/megapixel.js"></script>
            <script type="text/javascript" src="<?php echo PATH; ?>/js/include.js"></script>
            
<script>
  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/120776/script.js');
</script>
        <!-- ENDSCRIPT FILE -->
    	<?php wp_head(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111543846-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111543846-1');
</script>
    </head>

    <body <?php body_class(); ?>>
	<!--Header start-->
        <div class="header_bg">             
            <div class="support_online_bg hidden-xs hidden-sm">
                <div class="container">
                    <a href="tel:0905813582" class="hot_phone">0905.813.582</a>                
                    <a href="mailto:doduytuan2012@gmail.com.vn" class="hot_mail">doduytuan2012@gmail.com.vn</a>
                    <div class="support">        
                        <a href="https://www.facebook.com/datxanhmientrung8411/" title="Facebook" target="_blank" class="facebook"></a>        
                        
                        <a href="https://plus.google.com/111219241006996955894" title="Google +" target="_blank" class="google"></a>
                        <a href="https://www.youtube.com/channel/UCsGEjxuCBiK-FWzI3ufdN9Q" title="Youtube" target="_blank" class="youtube"></a>
                    </div>
                </div>
            </div>

            <div class="home_menu_bg hidden-xs hidden-sm" >
                <div class="container">                
                    <a href="<?php echo URL; ?>" class="logo">
                        <img src="<?php echo PATH; ?>/images/logo-new.png" height="42px"/>
                        <span class="text-logo">ĐỖ DUY TUẤN</span>
                    </a>
                    <div class="main_menu_bg">
                        <nav class="cl-effect-17">
                            <div class="menu_item">
                                <a href="<?php echo URL; ?>/lien-he">LIÊN HỆ</a>
                            </div>
                            <div class="menu_item">
                                <a href="<?php echo URL; ?>/tuyen-dung">TUYỂN DỤNG</a>
                            </div>
                            <!-- <div class="menu_item">
                                <a href="<?php echo URL; ?>/thu-vien">THƯ VIỆN</a>
                            </div> -->
                            <div class="menu_item parent_menu_item"> 
                                <a href="<?php echo URL; ?>/tin-tuc">Tin tức</a>
                                <div class="sub_menu_bg">
                                    <div class="sub_menu_item_bg">
                                        <?php 
                                            $chuyenmuc_object = get_terms( array(
                                                'taxonomy' => 'chuyen-muc',
                                                'hide_empty' => false,
                                            ));
                                        ?>
                                        <?php foreach ($chuyenmuc_object as $chuyenmuc) : $id_chuyenmuc = $chuyenmuc->term_id; ?>
                                            <a href="<?php echo get_term_link($id_chuyenmuc); ?>"><?php echo $chuyenmuc->name; ?></a>
                                        <?php endforeach; ?>                                         
                                    </div>
                                </div>
                            </div>
                            <div class="menu_item parent_menu_item">
                                <a href="<?php echo URL; ?>/du-an">Dự án</a>
                                <div class="sub_menu_bg">
                                    <div class="sub_menu_item_bg"> 
                                        <?php 
                                            $danhmuc_object = get_terms( array(
                                                'taxonomy' => 'danh-muc',
                                                'hide_empty' => false,
                                            ));
                                        ?>
                                        <?php foreach ($danhmuc_object as $danhmuc) : $id_danhmuc = $danhmuc->term_id; ?>
                                            <a href="<?php echo get_term_link($id_danhmuc); ?>">
                                                <?php echo $danhmuc->name; ?>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>     
                            <div class="menu_item">               
                                <a href="<?php echo URL; ?>/gioi-thieu">GIỚI THIỆU</a>                        
                            </div>
                            <div class="menu_item selected">
                                <a href="<?php echo URL; ?>" class="selected">TRANG CHỦ</a>
                            </div>
                        </nav>                
                    </div>
                </div>                                                       
            </div>  
            
            <div class="support_online_bg visible-xs visible-sm">
                <div class="container">
                    <a href="tel:0905813582" class="hot_phone">0905.813.582</a>                                            
                    <a href="mailto:doduytuan2012@gmail.com.vn" class="hot_mail">doduytuan2012@gmail.com.vn</a>       
                </div>
            </div>
            <div class="header_menu_bg_m visible-xs visible-sm">
                <a href="<?php echo URL; ?>" class="mobile logo">
                    <img src="<?php echo PATH; ?>/images/logo-new_m.png"height="32px"/>
                    <span class="text-logo_m">ĐỖ DUY TUẤN</span>
                </a>
                <a class="menu_btn_m"></a>      
                <div class="main_menu_bg_m">                          
                    <a class="menu_item" href="<?php echo URL; ?>" class="selected">TRANG CHỦ</a>
                    <a class="menu_item" href="<?php echo URL; ?>/gioi-thieu">GIỚI THIỆU</a>
                    <a class="menu_item" href="<?php echo URL; ?>/du-an">Dự án</a>
                    <a class="menu_item" href="<?php echo URL; ?>/tin-tuc">Tin tức</a>
                    <a class="menu_item" href="<?php echo URL; ?>/thu-vien">THƯ VIỆN</a>
                    <a class="menu_item" href="<?php echo URL; ?>/tuyen-dung">TUYỂN DỤNG</a>
                    <a class="menu_item" href="<?php echo URL; ?>/lien-he">LIÊN HỆ</a>                   
                </div>            
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('.menu_btn_m').click(function() {
                    $(this).toggleClass('active');
                    $('.main_menu_bg_m').toggle();
                });
            })
        </script>
    <!--Header END-->
    <!-- Start main body -->
    <main id="main-body">
