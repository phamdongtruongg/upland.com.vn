<?php get_header(); ?>
	<div class="body_bg background_white">
        <div class="page_img margin_top_page_mb">
            <div class="wallpaper_img"></div>
            <img src="<?php echo IMAGES; ?>/duan/Project_01.jpg">
        </div>
        <div class="container page_bg padding_bottom">                
            <div class="project_menu col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="fixed_menu_bg" style="position: static; width: 255px; top: 95px;">
                    <div class="split_bar_news_top"></div>
                    <div class="left_menu_bg"> 

                        <?php 
                            $danhmuc_object = get_terms( array(
                                'taxonomy' => 'danh-muc',
                                'hide_empty' => false,
                            ));
                        ?>
                        <?php foreach ($danhmuc_object as $danhmuc) : $id_danhmuc = $danhmuc->term_id; ?>
                        
                            <a href="<?php echo get_term_link($id_danhmuc); ?>" class="left_menu_category_item project_menu_item1 " rel="cid">
                                <?php echo $danhmuc->name; ?><span></span>
                            </a>
                            
                        <?php endforeach; ?>

                    </div>
                    <div class="hotline_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs hidden-sm">
                        <div class="img_hotline"></div>
                        <div class="info_hotline">
                            Hotline: <a href="tel:0905813582"><span>0905.813.582</span></a><br>
                            <a href="mailto:doduytuan2012@gmail.com.vn">doduytuan2012@gmail.com.vn</a>
                        </div>
                    </div>
                </div>
            </div>           
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 margin_top_mb">          
                <div class="page_title">
                    <a href="#!Project/Du-an.mpx">Dự án</a>    
                </div>
                <div class="page_content_detail">
                    <?php while (have_posts()) : the_post(); $duan_id = get_the_ID(); ?>
                        <div class="project_item_bg">
                            <a href="<?php the_permalink(); ?>" class="project_img col-lg-5 col-md-6 col-sm-12 col-xs-12 no_padding_right">
                                <img src="<?php echo get_the_post_thumbnail_url($duan_id); ?>">
                            </a>
                            <div class="project_info col-lg-7 col-md-6 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" class="project_title"><?php the_title(); ?></a>
                                <div class="project_location col-lg-5 col-md-5 col-sm-5 col-xs-12 no_padding_right">Đà Nẵng</div>
                            
                                <div class="split_bar_project"></div>
                                <div class="project_content">
                                    <?php the_excerpt(); ?>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="readmore_link visible-lg">Xem thêm</a>                    
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div> 
            </div>
            
            <?php include_once 'inc/template-pagination.php'; ?>

        </div>  
    </div>
<?php get_footer(); ?>