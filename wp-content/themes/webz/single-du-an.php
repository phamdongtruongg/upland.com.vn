<?php get_header(); ?>
<div class="body_bg background_white">

    <div class="page_img margin_top_page_mb">
        <div class="wallpaper_img"></div>
        <img src="<?php echo IMAGES; ?>/BN-PHU.jpg">
    </div>
    
    <div class="container page_bg padding_bottom">
        
        <div class="project_menu col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <div class="fixed_menu_bg" style="position: static; width : 280px;">                                     
                <div class="split_bar_news_top"></div>
                <div class="left_menu_project_bg">
                    <a class="left_menu_project_item selected nav-intro">Giới thiệu<span></span></a>
                    <a class="left_menu_project_item">Tổng quan dự án<span></span></a>
                    <a class="left_menu_project_item">Vị trí<span></span></a>
                    <a class="left_menu_project_item">Tiện ích<span></span></a>
                    <a class="left_menu_project_item">Tiến độ<span></span></a>
                </div>
                <div class="hotline_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="img_hotline"></div>
                    <div class="info_hotline">
                        Hotline: <a href="tel:0905813582"><span>0905.813.582</span></a><br>
                        <a href="mailto:doduytuan2012@gmail.com.vn">doduytuan2012@gmail.com.vn</a>
                    </div>
                </div>    
            </div> 
        </div>  
        <?php while (have_posts()) : the_post(); $post_id = get_the_ID(); ?>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            
            <div class="tab_content_item_bg" style="display: block;"> 
                <div class="page_title"><a href="<?php echo URL; ?>/du-an">Dự án</a> <span></span><?php the_title(); ?></div>
                <div class="page_content">
                    <div class="page_break"></div>
                    <div class="slider-wrapper theme-default project_slide_bg"></div>
                </div>
            </div>
            
            <div class="page_content_detail project">                      
                <?php the_content(); ?> 
            </div>
        
        <?php 
            $danh_muc = get_the_terms( $post_id, 'danh-muc');
            $args = array(
                'post_type' => 'du-an',
                'posts_per_page' => 5,
                'post__not_in' => array($post_id),
                'orderby'          => 'date',
                'order'            => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'danh-muc',
                        'field' => 'term_id',
                        'terms' => $danh_muc[0]->term_id
                    )
                )
            );
            $projectlist = get_posts( $args );
        ?>
        <?php if (!empty($projectlist)) : ?>
            <div class="news_other_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding_right">
                <div class="split_bar_news_top"><div class="news_other_title">Các dự án khác</div></div>
                <div class="news_other_item_bg">

                    <?php foreach ($projectlist as $project) : ?>
                        <a href="<?php echo get_the_permalink($project->ID); ?>" class="bubble-float-right "><?php echo $project->post_title; ?><span> ( <?php echo get_the_time('d/m/Y', $project->post_ID ); ?> )</span></a><br class="">
                    <?php endforeach; ?>
                </div>
                <a href="<?php echo URL; ?>/du-an" class="other_new_readmore_btn">Xem thêm</a>  

            </div>
        <?php endif; ?>
        </div>
        <?php endwhile; ?>
    </div>    
</div>
 <script>
    $(document).ready(function(){
        $(window).scroll(function(){
            var x = $(window).scrollTop();
            console.log(x);
            if(x > 540){
                $('.fixed_menu_bg').css({'position':'fixed', 'top':'100px', 'width' : '280px'});
            } else{
                $('.fixed_menu_bg').css({'position':'static', });
            }
        });
        $('.left_menu_project_item').click(function() {
            //$('.left_menu_project_item').removeClass('selected');
            //$(this).toggleClass('selected');
            // var x = $(this).attr('data-id');
            // $('.tab_content_item_bg').css('display','none');
            // $(".tab_content_item_bg:eq("+x+")").css('display','block');
        });

        $('.left_menu_project_item').click(function () {
            index = $(this).index('.left_menu_project_item');
            console.log(index);
            $('.title:eq(' + index + ')').scrollTo(500);
            $('.left_menu_project_item').removeClass('selected');
            $(this).addClass('selected');      
        });

    })
</script> 
<?php get_footer(); ?>