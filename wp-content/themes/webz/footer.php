	</main>
	<!-- end main body -->
	<!--Footer start-->
    <div class="footer_bg">
        <div class="container">
            <div class="footer_info col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="<?php bloginfo('url'); ?>" class="google_map footer_info_title">Chuyên gia tư vấn bất động sản Đỗ Duy Tuấn <span></span></a>
                <div class="footer_info_content">
                    <span class="hidden-xs">Địa chỉ:</span> 438 đường 2-9, P. Hòa Cường Bắc, Q.Hải Châu, TP.Đà Nẵng<br />
                    Tel: 0905 813 582 - 0901 997 046 </br>
                    Email: doduytuan2012@gmail.com.vn</br>
                    Website: doduytuanbds.com
                </div>
            </div>
            <div class="footer_menu col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden-xs hidden-sm">
                <a href="">Hỗ trợ khách hàng</a><br/>
                <a href="">Sơ đồ website</a><br/>
                <a href="<?php echo URL; ?>/lien-he">LIÊN HỆ</a><br/>
            </div>
        </div>
        
        
        <div class="container">
            <div class="copy_right col-lg-6 col-md-6 col-sm-12 col-xs-12">           
                Copyright© 2017 <span>by WebZ</span>. <br class="visible-xs" />              
            </div>
            
        </div>
    </div>
    <!--Footer END-->
    <a class="to_top_btn" style="display: none;"></a>
<?php wp_footer(); ?>
    <script>
        var hrefPage = window.location.href;
        $.each($('.menu_item'), function(){
            urlPage  = $(this).find('a').attr('href');
            if(hrefPage == urlPage){
                $(this).find('a').addClass('selected');
            }else{
                $(this).find('a').removeClass('selected');
                $('.menu_item').eq($('.menu_item').length - 1).find('a').addClass('selected');
            }
        });
            
        
            
        
    </script>
</body>
</html>
