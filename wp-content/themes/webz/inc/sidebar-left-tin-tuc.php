<div class="project_menu col-lg-3 col-md-3 col-sm-4 col-xs-12">
    <div class="fixed_menu_bg" style="position: static; width: 255px; top: 95px;">                                       
        <div class="split_bar_news_top"></div>
        <div class="left_menu_bg">
            <?php 
                $chuyenmuc_object = get_terms( array(
				    'taxonomy' => 'chuyen-muc',
				    'hide_empty' => false,
				));
            ?>
            <?php foreach ($chuyenmuc_object as $chuyenmuc) : $id_chuyenmuc = $chuyenmuc->term_id; ?>
            	<a href="<?php echo get_term_link($id_chuyenmuc); ?>" class="left_menu_category_item news_menu_item" rel="cid"><?php echo $chuyenmuc->name; ?><span></span></a>
            <?php endforeach; ?>
            
        </div>
    </div>
</div>