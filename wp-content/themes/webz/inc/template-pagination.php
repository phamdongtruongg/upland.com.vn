
<?php 
	global $wp_query;
	$query = $wp_query->query['paged'];
    $total_page = $wp_query->max_num_pages;
    $paged = get_query_var('paged');
    $paged = (get_query_var('paged') && get_query_var('paged') != 0 ) ? get_query_var('paged') : 1;
    if ($total_page <= 1 ) return;
    if ($total_page <= 5 ) {
    	for ($i=1; $i <= $total_page ; $i++) { 
    		$link[] = $i;
    	}
    } else {
    	if ($paged <= 3) {
    		for ($i=1; $i <= 5 ; $i++) { 
        		$link[] = $i;
        	}
    	} else if ( $paged > 3 && $paged < $total_page-2 ){
    		for ($i= $paged -2 ; $i <= $paged+ 2 ; $i++) { 
        		$link[] = $i;
        	}
    	} else {
    		for ($i= $total_page - 2 ; $i <= $total_page+ 2 ; $i++) { 
        		$link[] = $i;
        	}
    	}
    }
?>
<div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-12 col-xs-12">
    <div class="split_bar_page_bottom"></div>                             
    <div class="page_navigation_bg">
    	<?php 
        	for ($i=1; $i <= count($link); $i++) :
      	?>
      		<a href="<?php  echo get_pagenum_link($i); ?>" 
      			<?php if ($paged == $i) { echo 'class="selected"';} ?> ><?php echo $i; ?>
      			
      		</a>
      <?php endfor; ?>
        <!-- <a href="" class="selected">1</a>
        <a href="">2</a>
        <a href="">3</a>
        <a href="">4</a>
        <a href="">5</a> -->
    </div>                      
</div>