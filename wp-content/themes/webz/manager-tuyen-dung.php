<?php 
	add_action('init','create_custom_post_type_tuyen_dung');
	function create_custom_post_type_tuyen_dung(){
		$args = array(
			'labels' => array(
	           	'name' => __( 'Tuyển dụng' ),
	            'singular_name' => __( 'tuyển dụng' )
	        ),
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail'
			),
			'rewrite' => array('slug' => 'tuyen-dung', 'with_front' => true)
		);
	   register_post_type('tuyen-dung', $args);

	//    $args_taxonomy = array(
	// 		'hierarchical' => true,
	// 		'label' => 'Chuyên mục',
	// 		'rewrite' => true,
	// 		'slug' => 'chuyen-muc'
	// 	);
		register_taxonomy('chuyen-muc',array('tuyen-dung'),$args_taxonomy);
	}
