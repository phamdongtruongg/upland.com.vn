<?php 
/*
Template Name: Tuyển dụng
*/
        $args = array(
                'post_type' => 'tuyen-dung', 
                'showposts' => '100',
                'order' => 'ASC', 
                'orderby' => 'ID',                          
            );
    $tuyen_dung = new WP_Query($args);
   $tuyen_dung = $tuyen_dung->posts;
     ?>
<?php get_header(); ?>
	<div class="body_bg background_white">
        <div class="page_img margin_top_page_mb">
            <div class="wallpaper_img"></div>
            <img src="<?php echo IMAGES; ?>/duan/Project_01.jpg">
        </div>
        <div class="container page_bg padding_bottom">                
            <div class="project_menu col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="fixed_menu_bg" style="position: static; width: 255px; top: 95px;">
                <div class="hotline_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs hidden-sm">
                        <div class="img_hotline"></div>
                        <div class="info_hotline">
                            Hotline: <a href="tel:0905813582"><span>0905.813.582</span></a><br>
                            <a href="mailto:doduytuan2012@gmail.com.vn">doduytuan2012@gmail.com.vn</a>
                        </div>
                    </div>
                </div>
            </div>           
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 margin_top_mb">          
                <div class="page_title">
                    <a href="#!Project/Du-an.mpx">Tuyển dụng</a>    
                </div>
                <div class="page_content_detail">
                   <?php foreach ($tuyen_dung  as $tuyendung ) :  ?>
                        <div class="project_item_bg">
                            <a href="<?php echo get_the_permalink($tuyendung->ID); ?>" class="project_img col-lg-5 col-md-6 col-sm-12 col-xs-12 no_padding_right">
                                <img src="<?php  echo get_the_post_thumbnail_url($tuyendung->ID); ?>">
                            </a>
                            <div class="project_info col-lg-7 col-md-6 col-sm-12 col-xs-12">
                                <a href="<?php echo get_the_permalink($tuyendung->ID); ?>" class="project_title"><?php echo  $tuyendung->post_title; ?></a>
                                	
                                <div class="split_bar_project"></div>
                                <div class="project_content">
                                    <?php  $content = substr($tuyendung->post_content, 0, 340); 
                                    		$content = explode( ' ', $content) ;
                                    		$countContent = count($content);
                                       $content = array_splice($content, 1, $countContent - 2); 
                                      	echo $content = implode(' ', $content);
                                      ?>
                                </div>
									 <a href="<?php echo get_the_permalink($tuyendung->ID); ?>" class="readmore_link visible-lg">Xem thêm</a>
                                                   
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div> 
            </div>
            
            <?php include_once 'inc/template-pagination.php'; ?>
	
        </div> 
    </div>
<?php get_footer(); ?>
   