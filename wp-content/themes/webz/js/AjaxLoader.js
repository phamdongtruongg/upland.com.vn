﻿$(document).ready(function () {
    LoadPage();
    MobileMenu();
    //LanguageSelect();
    //    var imgLoad = imagesLoaded('body');
    //    imgLoad.on('always', function () {
    //        LoadPage();
    //        MobileMenu();
    //    });
});
$(function () {
    $(window).off().hashchange(function () {
        if (location.hash.length > 1) {
            LoadPage();
            //LoadSlider('.home_slider');
        }
    });
    $(window).hashchange();
});
//-----------------------------------------------Load Page----------------------------------------------//
function LoadPage() {

    NProgress.start();
    //$('.loading').fadeIn();
    var body = $('.page_ajax');
    var page = ResolveUrl("~/Page/");
    var url = window.location.href;
    if (url.indexOf("Home/") > 0 || url.indexOf(".mpx") < 0) {
        page += 'Home.aspx';
    }
    else if (url.indexOf("About/") > 0) {
        if (url.indexOf("/SID") > 0) {
            sid = QueryString(url, "/SID");
            page += 'AboutDetails.aspx?sid=' + sid;
        } else {
            page += 'About.aspx';
        }
    }
    else if (url.indexOf("News/") > 0) {
        var page_index = "1";
        if (url.indexOf("/CID") > 0 && url.indexOf("/SID") > 0) {
            cid = QueryString(url, "/CID");
            sid = QueryString(url, "/SID");
            page += 'NewsDetailsSID.aspx?cid=' + cid + '&sid=' + sid;
        } 
        else if (url.indexOf("/CID") > 0 && url.indexOf("/PI") < 0) {
            cid = QueryString(url, "/CID");
            page += 'News.aspx?cid=' + cid;
        }
        else if (url.indexOf("/CID") < 0 && url.indexOf("/PI") > 0) {
            page_index = QueryString(url, "/PI");
            page += 'News.aspx?pi=' + page_index;
        }
        else if (url.indexOf("/CID") > 0 && url.indexOf("/PI") > 0) {            
            cid = QueryString(url, "/CID");
            page_index = QueryString(url, "/PI");
            page += 'News.aspx?cid=' + cid + '&pi=' + page_index;
        }
        else {
            page += 'News.aspx';
        }
    }
    else if (url.indexOf("Gallery/") > 0) {
        page += 'Gallery.aspx';
    }
    else if (url.indexOf("Careers/") > 0) {
        if (url.indexOf("/SID") > 0) {
            sid = QueryString(url, "/SID");
            page += 'RecruitmentDetails.aspx?sid=' + sid;
        } else {
            page += 'Recruitment.aspx';
        }
    }
    else if (url.indexOf("CareersInfo/") > 0) {
        page += 'RecruitmentInfo.aspx';
    }    
    else if (url.indexOf("Support/") > 0) {
        page += 'Support.aspx';
    }
    else if (url.indexOf("Contact/") > 0) {
        page += 'Contact.aspx';
    }
    else if (url.indexOf("Project/") > 0) {
        var page_index = "1";
        if (url.indexOf("/SID") > 0) {
            sid = QueryString(url, "/SID");
            page += 'ServicesDetails.aspx?sid=' + sid;            
        }
        else if (url.indexOf("/CID") > 0 && url.indexOf("/PI") < 0)
        {
            cid = QueryString(url, "/CID");
            page += 'Services.aspx?cid=' + cid;
        }
        else if (url.indexOf("/CID") < 0 && url.indexOf("/PI") > 0) {
            page_index = QueryString(url, "/PI");
            page += 'Services.aspx?pi=' + page_index;
        }
        else if (url.indexOf("/CID") > 0 && url.indexOf("/PI") > 0) {
            cid = QueryString(url, "/CID");
            page_index = QueryString(url, "/PI");
            page += 'Services.aspx?cid=' + cid + '&pi=' + page_index;
        }
        else{
            page += 'Services.aspx';
        }
    }
    else if (url.indexOf("Sitemap/") > 0) {
        page += 'Sitemap.aspx';
    }
    else if (url.indexOf("Events/") > 0) {
        page += 'Events.aspx';
    }
    $('.page_ajax').transition({ opacity: 0 }, 500, function () {
        $('body').animate({ scrollTop: 0 }, 0, function () {
            //$('html, body').animate({ scrollTop: 0 }, 0);
            LoadPageAjax(body, page);
            $.ajaxSetup({ cache: false });
        });
    });    
}
//-----------------------------------------------Load Page Ajax----------------------------------------------//
var pdb_Top = $(".page_img img").height();
var menuW = $('.left_menu_item').width();
function LoadPageAjax(control, page) {
    //try {
    $(control).load(page, function (response, status, xhr) {
        CarouselControl();
        //Combobox('#contentMain_ddlRoom, #contentMain_ddlRoom1, #contentMain_ddlRoom2, #contentMain_ddlPayMethod');
        ShowGallery('.gallery_item, .room_img_view');
        SetCurrentMenuItem();
        ViewFullImage('.room_item_img, .page_detail_img, .page_bg .page_img');
        GoogleMap(".google_map");
        HomeLoad();
        AutoImageSrc('.project_slide_bg img', "DatXanh");

        if (status == "success") {
            pdb_Top = $(window).width() / 1366 * 380; //$(".page_img img").eq(0).height();
            menuW = $('.fixed_menu_bg').eq(0).width();
            SetCurrencyText('.project_price span');
            AboutTabs();
            //CareersTabs();
            ContactTabs();
            ProjectTabs();
            ProjectCarousel();
            OtherNewsReadmore();
            //EqualHeight('.project_info_close');
            NewMenuSelected();
            //MaxHeight('.project_item_content');
            GoogleMap();
            //NewItemImage();
            NProgress.done();
            $('.page_ajax').transition({ opacity: 1 }, 500);
            ProjectPageLoad();
            LoadVideo();
            //$('.loading').fadeOut();
            //equal_cols('.room_info, .dining_info, .news_item_content');
            //$('.gallery_item_hidden').appendTo(".hidden_gallery");
        }

    });
    //} catch (ex) { }
}
function LoadVideo() {
    url = window.location.href;
    if (url.indexOf("Project/SID89/NGO-QUYEN-SHOPPING-STREET.mpx") > 0) {
        OpenVideo('https://www.youtube.com/embed/Cyac5RT6a5M?autoplay=1');
    }
}