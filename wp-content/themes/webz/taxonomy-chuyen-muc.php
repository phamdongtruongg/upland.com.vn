 <?php get_header(); ?>
    <?php require_once 'inc/header-tin-tuc.php'; ?>

    <div class="container page_bg padding_bottom">     
        
        <?php include_once 'inc/sidebar-left-tin-tuc.php'; ?>

        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 margin_top_mb">
            <div class="page_title">
                <a href="#!News/Tin-tuc.mpx">Tin tức</a>            
            </div>
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) :  the_post() ; $new_id = get_the_ID();?>
                    <div class="page_content_detail"> 
                        <div class="news_item">
                            <a href="<?php the_permalink(); ?>" class="news_img col-lg-4 col-md-4 col-sm-12 col-xs-12 no_padding_right">
                                <img src="<?php echo get_the_post_thumbnail_url($new_id); ?>">
                            </a>
                            <div class="news_info col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" class="news_title">
                                    <?php the_title();  ?>
                                </a>
                                <div class="split_bar_news"></div>
                                <div class="news_content des_string">
                                    <div style="text-align: left;"></div>
                                    <div style="text-align: justify; ">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>                    
                            </div>                   
                        </div> 
                    </div>       
                <?php endwhile; ?>
            <?php else: ?>
                <p>Hiện chưa có bài viết nào</p>
            <?php endif; ?>
            
        </div>
        <?php include_once 'inc/template-pagination.php'; ?>

        <!-- <div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-12 col-xs-12">
            <div class="split_bar_page_bottom"></div>                             
            <div class="page_navigation_bg">
                <a href="#!News/PI1/Tin-tuc.mpx" class="selected">1</a>
                <a href="#!News/PI2/Tin-tuc.mpx">2</a>
                <a href="#!News/PI3/Tin-tuc.mpx">3</a>
                <a href="#!News/PI4/Tin-tuc.mpx">4</a>
                <a href="#!News/PI5/Tin-tuc.mpx">5</a>
            </div>                      
        </div> -->
    </div>
<?php get_footer(); ?>