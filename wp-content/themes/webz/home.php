<?php get_header(); ?>
<!-- start slider  -->
<div class="home_slider">
    <?php if ( is_active_sidebar( 'slide-homepage' ) ) : ?>
        <div id="secondary" class="widget-area" >
        <?php dynamic_sidebar( 'slide-homepage' ); ?>
        </div>
    <?php endif; ?>
</div>
<!-- end slider  -->

<!-- start dự án -->
<div class="container page_bg">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                       
        <div class="split_bar_news_top"><a href="#!Project/Du-an.mpx" class="title_home">Dự án</a></div>
    </div>            
    <div class="home_project_carousel carousel fdi-Carousel slide">
        <!-- Carousel items -->
        <div class="carousel fdi-Carousel slide" id="eventCarousel_D" data-interval="0">
            <div class="carousel-inner home_project">                   
                <div class="item active">
                    <?php 
                        $args = array(
                            'post_type' => 'du-an',
                            'posts_per_page' => 3,
                            'orderby'          => 'date',
                            'order'            => 'DESC',
                        );
                        $projectlist = get_posts( $args );
                    ?>
                    <?php foreach ($projectlist as $project) : ?>
                        <div class="home_project_item_bg col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a class="project_item_img" href="<?php echo get_the_permalink($project->ID); ?>"> 
                                <img src="<?php echo get_the_post_thumbnail_url($project->ID); ?>">
                                <div class="project_item_title"><?php echo $project->post_title; ?></div>                    
                            </a>
                            <!-- <div class="project_item_content"><div>
                                <?php  echo $project->post_excerpt; ?>
                            </div></div> -->
                        </div>  
                    <?php endforeach;  ?>
                    

                </div>                        
            </div>     
        </div><!--/carousel-inner-->
    </div><!--/myCarousel-->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                              
        <div class="split_bar_news_bottom">
            <div class="news_carousel carousel_background">
                <a class="other_new_readmore_btn" href="<?php echo URL; ?>/du-an">Xem thêm</a>
            </div>
        </div>                            
    </div>
</div>
<!-- end dự án -->

<!-- start tin tức -->
<div class="news_bg">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                       
            <div class="split_bar_news_top"><a href="#!News/Tin-tuc.mpx" class="title_home margin_top">Tin tức nổi bật</a></div>
        </div>
        <div id="home_new_carousel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 carousel slide carousel-fade " data-ride="carousel">
              <!-- Indicators -->           
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                
                <div class="item active">
                    <div class="news_silde_item_bg">
                        
                        <?php 
                            $args = array(
                                'post_type' => 'tin-tuc',
                                'posts_per_page' => 5,
                                'orderby'          => 'date',
                                'order'            => 'DESC',
                            );
                            $newslist = get_posts( $args );
                        ?>

                        <div class="news_item_bg col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <img src="<?php echo get_the_post_thumbnail_url($newslist[0]->ID); ?>" title="DỰ ÁN MARINA COMPLEX: MỘT NĂM NHÌN LẠI">
                            <a href="<?php echo get_the_permalink($newslist[0]->ID); ?>" class="news_item_title"><?php echo $newslist[0]->post_title; ?></a> 
                        </div>

                        <div class="no_padding_right col-lg-6 col-md-6 col-sm-12 col-xs-12 d-flex">
                            <?php 
                                foreach ($newslist as $key => $new) : 
                                if($key != 0 ) : 
                            ?>
                                <div class="news_sub_item_bg home_news_sub_m col-lg-6 col-md-6 col-sm-12 col-xs-12" style="height: 200px;">                           
                                    <img src="<?php echo get_the_post_thumbnail_url($new->ID); ?>" title="<?php echo $new->post_title; ?>" class="hidden-xs hidden-sm"> 
                                    <a href="<?php echo get_the_permalink($new->ID); ?>" class="news_item_title home_news_title_m">
                                        <?php echo $new->post_title; ?>
                                    </a>
                                </div>                          
                            <?php 
                                endif;
                                endforeach;
                            ?>     
                        </div>

                    </div>     
                </div>
                
                
              </div>
            </div>     
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                             
            <div class="split_bar_news_bottom">
                <div class="news_carousel">
                    <a class="other_new_readmore_btn" href="<?php echo URL; ?>/tin-tuc">Xem thêm</a>
                    
                </div>
            </div>                              
        </div>            
        <!--News mobite START-->
        
        <!--News mobite END-->
    </div>
</div>
<!-- end tin tức -->
<?php get_footer(); ?>