<?php 
	add_action('init','create_custom_post_type_tin_tuc');
	function create_custom_post_type_tin_tuc(){
		$args = array(
			'labels' => array(
	           	'name' => __( 'Quản lý tin tức' ),
	            'singular_name' => __( 'tin tức' )
	        ),
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail'
			),
			'rewrite' => array('slug' => 'tin-tuc', 'with_front' => true)
		);
	   register_post_type('tin-tuc', $args);

	   $args_taxonomy = array(
			'hierarchical' => true,
			'label' => 'Chuyên mục',
			'rewrite' => true,
			'slug' => 'chuyen-muc'
		);
		register_taxonomy('chuyen-muc',array('tin-tuc'),$args_taxonomy);
	}
