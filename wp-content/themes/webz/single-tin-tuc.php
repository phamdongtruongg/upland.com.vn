<?php get_header(); ?>
<div class="body_bg">
    <?php require_once 'inc/header-tin-tuc.php'; ?>
    <?php         
        while (have_posts()) : the_post(); 
        $post_id = get_the_ID();
        $chuyen_muc = get_the_terms( $post_id, 'chuyen-muc');
    ?>
        <div class="container page_bg padding_bottom">             
            
            <?php include_once 'inc/sidebar-left-tin-tuc.php'; ?>

            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                <div class="tab_content_item_bg" style="display: block;">
                    <div class="page_title">
                        <a href="<?php echo URL; ?>/tin-tuc">Tin tức</a>
                        <span></span>
                        <a href="<?php echo get_term_link($chuyen_muc[0]->term_id); ?>"><?php echo $chuyen_muc[0]->name; ?></a>
                    </div>
                         
                    <div class="page_content_detail">
                        <div class="news_title_details"><?php the_title(); ?></div>                  
                        <div class="about_content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php 
                    $args = array(
                        'post_type' => 'tin-tuc',
                        'posts_per_page' => 5,
                        'post__not_in' => array($post_id),
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'chuyen-muc',
                                'field' => 'term_id',
                                'terms' => $chuyen_muc[0]->term_id
                            )
                        )
                    );
                    $postslist = get_posts( $args );
                ?>
                <?php if (!empty($postslist)) : ?>
                    <div class="news_other_bg col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding_right">
                        <div class="split_bar_news_top"><div class="news_other_title">Các tin khác</div></div>
                        <div class="news_other_item_bg">
                            
                            <?php foreach ($postslist as $post) : ?>
                                <a href="<?php echo get_the_permalink($post->ID); ?>" class="bubble-float-right "><?php echo $post->post_title; ?><span> ( <?php echo get_the_time('d/m/Y', $post->post_ID ); ?> )</span></a><br class="">
                            <?php endforeach; ?>
                        </div>
                        <a href="<?php echo get_term_link($chuyen_muc[0]->term_id); ?>" class="other_new_readmore_btn">Xem thêm</a>        
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endwhile; ?>
</div>
<?php get_footer(); ?>