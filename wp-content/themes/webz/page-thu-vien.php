<?php get_header(); ?>
<div class="body_bg background_white">
    <div class="page_img margin_top_page_mb">
        <div class="wallpaper_img"></div>
        <img src="<?php echo IMAGES; ?>/Gallery_01.jpg">
    </div>
    <div class="container page_bg padding_bottom">     
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                    
            <div class="page_title">THƯ VIỆN</div>
            <div class="row gallery_item_bg">                         
                <a href="" caption="Trung tâm thương mại Ngô Quyền" rel="room_info_1" class="gallery_item room_img_view col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                    <div class="gallery_bg effect7">
                        <div class="gallery_img"><img src="<?php echo IMAGES; ?>/thuvien/1.jpg"></div>
                        <div class="gallery_info">
                             <div class="readmore_btn">
                            </div>              
                        </div>
                        <div class="gallery_title">Trung tâm thương mại Ngô Quyền</div>                              
                    </div>
                </a>
                
                    <a href="/uploads/Gallery/NQM-05.jpg" caption="Trung tâm thương mại Ngô Quyền" rel="room_info_1" class="gallery_item_hidden room_img_view"></a>
                
                    <a href="/uploads/Gallery/NQM-03.jpg" caption="Trung tâm thương mại Ngô Quyền" rel="room_info_1" class="gallery_item_hidden room_img_view"></a>
                
                    <a href="/uploads/Gallery/NQM-02.jpg" caption="Trung tâm thương mại Ngô Quyền" rel="room_info_1" class="gallery_item_hidden room_img_view"></a>
                
                    <a href="/uploads/Gallery/NQM-04.jpg" caption="Trung tâm thương mại Ngô Quyền" rel="room_info_1" class="gallery_item_hidden room_img_view"></a>
                
                    <a href="/uploads/Gallery/NQM-06.jpg" caption="Trung tâm thương mại Ngô Quyền" rel="room_info_1" class="gallery_item_hidden room_img_view"></a>
                

                <div class="hidden_gallery"></div>
            </div>
            
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                             
            <div class="split_bar_news_bottom"></div>                              
        </div>
    </div>
</div>
<?php get_footer(); ?>